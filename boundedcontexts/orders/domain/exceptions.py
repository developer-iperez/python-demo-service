class NoOrderFoundException(Exception):
    def __init__(self, order_id: int):
        self.order_id = order_id
        
class OrderAlreadyPurchasedException(Exception):
    def __init__(self, order_id: int):
        self.order_id = order_id
        
class OrderStatusUpdatedException(Exception):
    def __init__(self, order_id: int, old_status: int, new_status: int):
        self.order_id = order_id
        self.old_status = old_status
        self.new_status = new_status