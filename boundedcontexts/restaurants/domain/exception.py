class NoRestaurantFoundException(Exception):
    def __init__(self, restaurant_id: int):
        self.restaurant_id = restaurant_id