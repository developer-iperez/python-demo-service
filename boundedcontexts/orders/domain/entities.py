from enum import IntEnum

class OrderStatus(IntEnum):
    Created_Pay_Pending = 1
    Paid = 2
    In_Preparation = 3
    Ready_To_Be_Delivered = 4
    Sent = 5
    Delivered = 6
    Rejected = 7
    Cancelled = 8

class OrderPaymentStatus(IntEnum):
    Pending = 1
    Rejected = 2
    Paid = 3

class Order:
    id: int
    client_id: int
    restaurant_id: int
    menu_id: int
    delivery_address: str
    order_status: OrderStatus
    
class OrderPayment:
    id: int
    order_id: int
    statys: OrderPaymentStatus