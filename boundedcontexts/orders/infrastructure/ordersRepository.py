from typing import List
from boundedcontexts.orders.domain.entities import Order, OrderStatus

orders_db = []

class OrdersRepository:
    def listOrders(self) -> List[Order]:
         return orders_db
    
    def getOrder(self, id: int) -> Order:
        availableOrders = self.listOrders()
        filtered_orders = [order for order in availableOrders if order.id == id]
        return filtered_orders[0] if len(filtered_orders) > 0 else None
    
    def findOrdersByMenuId(self, menu_id: int) -> List[Order]:
        orders = []
        availableOrders = self.listOrders()
        
        for index, order in enumerate(availableOrders):
            if (order.menu_id == menu_id):
                orders.append(order)
        
        return orders
    
    def createOrder(self, order: Order) -> Order:
        order.id = len(orders_db) + 1
        orders_db.append(order)
        return order
    
    def updatedOrder(self, orderToBeUpdated: Order) -> Order:
        targetIndex = -1
        
        for index, order in enumerate(orders_db):
            if order.id == orderToBeUpdated.id:
                targetIndex = index
                break 
        
        if (targetIndex < 0):
            raise Exception("No order found to be updated")
        
        orders_db[targetIndex] = orderToBeUpdated
        return orderToBeUpdated
