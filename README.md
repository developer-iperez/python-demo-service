# Requerimientos

------------------------------------------------------------------------------------------------------------------
API para cifrar y descifrar mensajes de texto. 
- Cifrado: El servicio generará una clave de cifrado única para cada mensaje.
- Descifrado: Los mensajes cifrados previamente pueden ser descifrados mediante la API con la clave de descifrado correspondiente.
- Gestión de Claves: El sevicio incluirá una funcionalidad de gestión de claves que permitirá a los usuarios generar, almacenar y recuperar las claves de cifrado de forma segura.
- Auditoría y Registro: El sistema registrará todas las operaciones de cifrado y descifrado para fines de auditoría y seguimiento de actividad.

------------------------------------------------------------------------------------------------------------------
API para gestionar contraseñas
- Almacenamiento de Contraseñas: Los usuarios pueden utilizar la API para almacenar sus contraseñas de forma segura en el gestor de contraseñas vinculado a la aplicación.
- Recuperación de Contraseñas: Los usuarios pueden recuperar contraseñas almacenadas utilizando la API, proporcionando credenciales de autenticación válidas.
- Actualización de Contraseñas: Los usuarios pueden actualizar sus contraseñas almacenadas mediante la API.
- Generación de Contraseñas Seguras: La API puede generar contraseñas seguras según los criterios definidos por los usuarios.
- Autenticación de Dos Factores (2FA): La API puede ser compatible con la autenticación de dos factores para un acceso adicionalmente seguro.

- Idea:
	- La aplicación trabaja con un nombre y password por usuario.
	- El usuario de la plataforma registrar un usuario+password+plataforma, este grupo lo llamaremos 1AuthWeb.
	- El sistema genera un token que permite autenticarse en la plataforma deseada de forma directa.
	- El usuario puede autenticarse en el sitio web destino mediante dos formas:
		- API de autenticación con un token, el cual puede compartirse con otras personas
		- API de autenticación con el identificador de usuario y password
		
- APIS
	- API Registrar usuario
	- API Listar usuarios
	- API Actualizar datos del usuario (nombre y contraseña)
	- API Darse de baja el usuario
	- API Añadir nueva entrada 1AuthWeb (usuario+contraseña+sitio web) dado un usuario registrado
	- API Listar todos las entradas 1AuthWeb
	- API Eliminar una entrada 1AuthWeb
	- API Generar token de autenticación para una entrada 1AuthWeb
	- API Revocar todos los token para una entrada 1AuthWeb
	- API Autenticación de autenticación
		- Anónimamente mendiante token 1AuthWeb
		- Mediante credenciales de usuario

------------------------------------------------------------------------------------------------------------------
HIstorias de usuario:
- Cliente:
	- Puedo crear un pedido con los siguientes pasos: (x)
		1) Busco restaurante (x)
		2) Escojo un menú (x)
		3) Selecciono la dirección de entrega (x)
		4) Pago (x)
	- Puedo consultar el estado de mi pedido (creado, en preparación, preparado, enviado, entregado, rechazado, cancelado) (x)
	- Puedo consultar el historial de pedidos
- Restaurante:
	- Puedo listar los pedidos creados y pagados que hayan sido solicitados a mi restaurante (x)
	- Puedo actualizar el estado del pedido (en preparación, listo para ser entregado)
- Repartidor:
	- Puedo listar los pedidos que me han sido asignados
	- Puedo actualizar el estado del pedido (entregado, rechazado)
- Empresa:
	O- Puedo listar los restaurantes con los que trabajo y por cada uno de ellos los menús que ofrecen
	?- Puedo gestionar el pedido de un cliente asociando un restaurante, menú y cliente.
	- Puedo listar la lista repartidores dados de alta.
	- Puedo listar los pedidos creados y pendientes de asignar a un repartidor. 
	- Para un pedido listo para envío puedo asignarle un repartidor.
	O- Puedo consultar el estado de todos los pedidos (número de pedido, cliente, repartidor, restaurante, estado, fecha de creación, fecha de asignación a repartidor, fecha de entrega, fecha de finalización)
	O- Puedo consultar el historial de pedidos completo de todos los usuarios
	O- Puedo actualizar el estado del pedido (creado, en preparación, preparado, enviado, entregado, rechazado, cancelado)

API Aplicación de Entrega a Domicilio
- Crear pedido: lista de productos, dirección de entrega y contacto del cliente
- Buscar restaurantes: lista de restaurantes cercanos
- Información de menú: lista de menú, precios y disponibilidad
- Gestión de pagos: integración de pasarela de pagos
- Estado del pedido (consulta): preparación, envío y entrega
- Estado del pedido (gestión): preparación, envío y entrega
- Notificaciones y comunicación: notificación del estado del pedido al cliente.
- Gestión de repartidores: Asignar un repartidor a un pedido, rastrear su ubicación y mostrar detalles de entrega
- Historial de pedidos: usuarios pueden ver lista de pedidos entregados.

- APIS:
	- API Buscar restaurantes
	- API Lista menus, precios y disponibilidad dado un restaurante
	- API Crear un pedido asignando usuario, menu, dirección de entrega y contacto del cliente (pedido se crea en estado STATE_CREADO)
	- API Tramitación del pago del pedido (pedido pasa de STATE_PREPARADO->STATE_PAGADO_PENDIENTE_ENTREGA)
	- API Lista de pedidos pendientes de asignar
	- API Lista de pedidos asignados
	- API Consultar estado del pedido
	- API Listar repartidores disponibles (dados de alta y no asignados a un pedido)
	- API Asignar repartidor a un pedido (pedido pasa de STATE_PAGADO_PENDIENTE_ENTREGA->STATE_ASIGNADO_PENDIENTE_ENTREGA)
	- API Producto entregado, el repartidor se libera y el pedido cambia de estado (pedido pasa de STATE_ASIGNADO_PENDIENTE_ENTREGA a STATE_ENTREGADO)
	- API Historial de pedidos de un usuario
	
# Librerías usadas

dependency-injector=4.41.0
flask=2.3.3

#URLS

https://python-dependency-injector.ets-labs.org/tutorials/flask.html
https://flask.palletsprojects.com/en/2.3.x/logging/#injecting-request-information
https://flask.palletsprojects.com/en/2.3.x/quickstart/#a-minimal-application
