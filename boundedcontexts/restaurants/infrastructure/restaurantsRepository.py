from typing import List
from boundedcontexts.restaurants.domain.entities import Menu, Restaurant

restaurants_db = [{
    "id": 1, 
    "name": "restaurant_1"
}, {
    "id": 2, 
    "name": "restaurant_2"
}]

menus_db = [{
    "id": 1,
    "restaurant_id": 1,
    "name": "Menu Special Working Days"
}, {
    "id": 2,
    "restaurant_id": 1,
    "name": "Menu Weekend"
}, {
    "id": 3,
    "restaurant_id": 1,
    "name": "Menu Special Nights"
}, {
    "id": 4,
    "restaurant_id": 2,
    "name": "Basic Menu"
}, {
    "id": 5,
    "restaurant_id": 2,
    "name": "Deluxe Menu"
}, {
    "id": 6,
    "restaurant_id": 2,
    "name": "Total Trash Supreme Menu"
}]

class RestaurantsRepository:
    def listRestaurants(self) -> List[Restaurant]:
         return restaurants_db
     
    def getRestaurant(self, id: int) -> Restaurant:
        availableRestaurants = self.listRestaurants()
        filtered_restaurant = next(filter(lambda restaurant: restaurant['id'] == id, availableRestaurants), None)
        return filtered_restaurant
         
    def listMenus(self, restaurant_id: int) -> List[Menu]:
        restaurant = self.getRestaurant(restaurant_id)
        if restaurant == None:
            raise Exception("No restaurant found for id")
        
        filtered_restarant_menus = [m for m in menus_db if m['restaurant_id'] == restaurant['id']]
        return filtered_restarant_menus
    
    def getRestaurantMenu(self, restaurant_id: int, menu_id: int) -> Menu:
        available_menus = self.listMenus(restaurant_id)
        
        filtered_restarant_menu = [m for m in available_menus if m['id'] == menu_id]
        return filtered_restarant_menu[0] if len(filtered_restarant_menu) > 0 else None
        
        
        