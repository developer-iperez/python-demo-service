from boundedcontexts.restaurants.infrastructure.restaurantsRepository import RestaurantsRepository

class RestaurantService:
    def __init__(self, repository: RestaurantsRepository):
        self.repository = repository
                    
    def listRestaurants(self):
        return self.repository.listRestaurants()
    
    def listMenus(self, restauranId: int):
        return self.repository.listMenus(restauranId)