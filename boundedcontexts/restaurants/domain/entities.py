from typing import List

class Restaurant:
    id: int
    name: str
    address: str
    
class Menu:
    id: int
    restaurant_id: int
    name: str
