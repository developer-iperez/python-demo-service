from typing import List
from boundedcontexts.orders.domain.exceptions import NoOrderFoundException, OrderAlreadyPurchasedException
from boundedcontexts.orders.infrastructure.ordersRepository import OrdersRepository
from boundedcontexts.orders.domain.entities import Order, OrderStatus
from boundedcontexts.restaurants.infrastructure.restaurantsRepository import RestaurantsRepository

class OrderServices:
    def __init__(self, restaurantsRepository: RestaurantsRepository, ordersRepository: OrdersRepository):
        self.restaurantsRepository = restaurantsRepository
        self.ordersRepository = ordersRepository
    
    def listOrders(self) -> List[Order]:
        return self.ordersRepository.listOrders()
    
    def createOrder(self, client_id: int, restaurant_id: int, menu_id: int, delivery_address: str) -> Order:
        menu = self.restaurantsRepository.getRestaurantMenu(restaurant_id, menu_id)
        if (menu == None):
            raise Exception("Menu not found")
        
        order = Order()
        order.client_id = client_id
        order.restaurant_id = restaurant_id
        order.menu_id = menu_id
        order.delivery_address = delivery_address
        order.order_status = int(OrderStatus.Created_Pay_Pending)

        createdOrder = self.ordersRepository.createOrder(order)
        return createdOrder
    
    def purchaseOrder(self, order_id: int) -> Order:
        existingOrder = self.ordersRepository.getOrder(order_id)
        if existingOrder == None:
            raise NoOrderFoundException(order_id)
        
        if (existingOrder.order_status == OrderStatus.Paid):
            raise OrderAlreadyPurchasedException(existingOrder.id)
        
        existingOrder.order_status = int(OrderStatus.Paid)
        return self.ordersRepository.updatedOrder(existingOrder)
    
    def orderStatus(self, order_id: int) -> int:
        existingOrder = self.ordersRepository.getOrder(order_id)
        if existingOrder == None:
            raise NoOrderFoundException(order_id)
        
        return int(existingOrder.order_status)
        