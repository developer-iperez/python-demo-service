from typing import List
from boundedcontexts.orders.domain.entities import Order, OrderStatus
from boundedcontexts.orders.domain.exceptions import NoOrderFoundException, OrderStatusUpdatedException
from boundedcontexts.orders.infrastructure.ordersRepository import OrdersRepository
from boundedcontexts.restaurants.domain.exception import NoRestaurantFoundException
from boundedcontexts.restaurants.infrastructure.restaurantsRepository import RestaurantsRepository

class OrdersPreparationService:
    def __init__(self, restaurantsRepository: RestaurantsRepository, ordersRepository: OrdersRepository):
        self.restaurantsRepository = restaurantsRepository
        self.ordersRepository = ordersRepository
        
    def listOrdersByAssignedRestaurant(self, restaurant_id: int) -> List[Order]:
        restaurant = self.restaurantsRepository.getRestaurant(restaurant_id)
        if (restaurant == None):
            raise NoRestaurantFoundException(restaurant_id)
        
        try:
            available_menus = self.restaurantsRepository.listMenus(restaurant['id'])
        except:
            available_menus = []    
        
        available_orders_for_restaurant = []
                
        for index, menu in enumerate(available_menus):
            order_menus = self.ordersRepository.findOrdersByMenuId(menu['id'])
            if (len(order_menus) > 0):
                available_orders_for_restaurant.extend(order_menus)
        
        return available_orders_for_restaurant
    
    def updateStatusOfRestaurantAssignedOrder(self, restaurant_id: int, order_id: int, status_to_update: int):
        order = self.ordersRepository.getOrder(order_id)
        if (order == None):
            raise NoOrderFoundException(order_id)
        
        if order.restaurant_id != restaurant_id:
            raise NoRestaurantFoundException(restaurant_id)
        
        if (self.validateRestaurantOrderStatusUpdate(order.order_status, status_to_update) == False):
            raise OrderStatusUpdatedException(order_id, order.order_status, status_to_update)
        
        order.order_status = status_to_update
        return self.ordersRepository.updatedOrder(order)
    
    #TODO: TO BE TESTED
    def validateRestaurantOrderStatusUpdate(seld, old_status: int, new_status: int):       
        if (old_status not in [OrderStatus.Paid, OrderStatus.In_Preparation, OrderStatus.Ready_To_Be_Delivered]):
            return False
        if (new_status not in [OrderStatus.Paid, OrderStatus.In_Preparation, OrderStatus.Ready_To_Be_Delivered]):
            return False
        
        if (old_status == OrderStatus.Paid):
            if (new_status == OrderStatus.Paid):
                return False
            else:
                return True
        elif (old_status == OrderStatus.In_Preparation):
            if (new_status == OrderStatus.Paid or new_status == OrderStatus.In_Preparation):
                return False
            else:
                return True
        else:
            return False        
