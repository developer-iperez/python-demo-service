""" Application module """

from flask import Flask, Response, jsonify, request
from boundedcontexts.orders.application.ordersPreparationService import OrdersPreparationService
from boundedcontexts.orders.domain.exceptions import NoOrderFoundException, OrderAlreadyPurchasedException
from boundedcontexts.orders.infrastructure.ordersRepository import OrdersRepository
from boundedcontexts.orders.application.ordersServices import OrderServices
from boundedcontexts.restaurants.application.restaurantsService import RestaurantService
from boundedcontexts.restaurants.infrastructure.restaurantsRepository import RestaurantsRepository
from .containers import Container

restaurantsRespository = RestaurantsRepository()
restaurantsService = RestaurantService(restaurantsRespository)
ordersRepository = OrdersRepository()
ordersService = OrderServices(restaurantsRespository, ordersRepository)
ordersPreparationService = OrdersPreparationService(restaurantsRespository, ordersRepository)

# Create a Flask application instance. The variable __name__ determine the location of the application.
app = Flask(__name__)

# - API Buscar restaurantes
# - API Lista menus, precios y disponibilidad dado un restaurante
# - API Crear un pedido asignando usuario, menu, dirección de entrega y contacto del cliente (pedido se crea en estado STATE_CREADO)
# - API Tramitación del pago del pedido (pedido pasa de STATE_PREPARADO->STATE_PAGADO_PENDIENTE_ENTREGA)
# - API Lista de pedidos pendientes de asignar
# - API Lista de pedidos asignados
# - API Consultar estado del pedido
# - API Listar repartidores disponibles (dados de alta y no asignados a un pedido)
# - API Asignar repartidor a un pedido (pedido pasa de STATE_PAGADO_PENDIENTE_ENTREGA->STATE_ASIGNADO_PENDIENTE_ENTREGA)
# - API Producto entregado, el repartidor se libera y el pedido cambia de estado (pedido pasa de STATE_ASIGNADO_PENDIENTE_ENTREGA a STATE_ENTREGADO)
# - API Historial de pedidos de un usuario

#http://127.0.0.1:5000/restaurants
@app.get('/restaurants')
def searchRestaurant():
    restaurants = restaurantsService.listRestaurants()
    return restaurants

#http://127.0.0.1:5000/restaurant/1/menus
@app.get('/restaurants/<restaurant_id>/menus')
def searchRestaurantMenu(restaurant_id: int):   
    try:
        menus = restaurantsService.listMenus(int(restaurant_id))
        return menus
    except Exception as e:
        return "Record not found", 400

#http://127.0.0.1:5000/restaurants/1/orders
@app.get('/restaurants/<restaurant_id>/orders')
def searchRestaurantOrders(restaurant_id: int):
    try:
        orders = ordersPreparationService.listOrdersByAssignedRestaurant(int(restaurant_id))
        return jsonify([order.__dict__ for order in orders])
    except Exception as e:
        return "Record not found", 400

#http://127.0.0.1:5000/restaurants/1/orders/1?updatedStatus=1
@app.put('/restaurants/<restaurant_id>/orders/<order_id>')
def updateRestaurantOrderStatus(restaurant_id: int, order_id: int):
    args = request.args
    updated_status = args.get('updatedStatus')
    
    try:
        updatedOrder = ordersPreparationService.updateStatusOfRestaurantAssignedOrder(int(restaurant_id), int(order_id), int(updated_status))
        return { 'id': updatedOrder.id }, 200
    except Exception as e:
        return "Record status couldn't be updated", 404

#http://127.0.0.1:5000/orders
@app.get('/orders')
def listOrders():
    orders = ordersService.listOrders()
    return jsonify([order.__dict__ for order in orders])

#http://127.0.0.1:5000/order?userId=1&menuId=1&address=my%20street,%2012
@app.post('/order')
def createOrder():
    args = request.args
    user_id = args.get('userId')
    restaurant_id = args.get('restaurantId')
    menu_id = args.get('menuId')
    address = args.get('address')
   
    try:
        createdOrder = ordersService.createOrder(int(user_id), int(restaurant_id), int(menu_id), address)
        return { 'id': createdOrder.id }, 200
    except Exception as e:
        return "Record not found", 400

#http://127.0.0.1:5000/order/1/purchase
@app.put('/order/<order_id>/purchase')
def purchaseOrder(order_id: int): 
    try:
        updatedOrder = ordersService.purchaseOrder(int(order_id))
        return { 'id': updatedOrder.id }, 200
    except NoOrderFoundException as e:
        return "Order not found", 404
    except OrderAlreadyPurchasedException as e:
        return "Order already purchased", 400
    except Exception as e:
        return "Error purchasing order", 500

#http://127.0.0.1:5000/order/1/status
@app.get('/order/<order_id>/status')
def orderStatus(order_id: int): 
    try:
        order_status = ordersService.orderStatus(int(order_id))
        return { 'id': order_id, 'status': order_status }, 200
    except NoOrderFoundException as e:
        return "Order not found", 404
    except Exception as e:
        return "Error purchasing order", 500

def index():
    return 'Demo Python+Flask API server'

def create_app() -> Flask:
    container = Container()
    
    app.container = container
    app.add_url_rule("/", "index", index)
    app.run()
