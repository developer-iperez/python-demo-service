from http import client
import json

host = "127.0.0.1"
port = 5000

print("Select an option:")
print(" 1) List restaurants")
print(" 2) Show restaurant information")
print(" 3) Order")

try:
    select = int(input())
except ValueError:
    print("Invalid option")
    
connection = client.HTTPConnection(host, port, timeout=10)

try:
    request = connection.request("GET", "/restaurants")
    response = connection.getresponse()

    if response.status == 200:
        json_data = json.loads(response.read().decode('utf-8'))
        print(json_data)
    else:
        print("Invalid request")
except Exception as exception:
    print(f"Exception thrown, cause: {exception}")    
finally:
    connection.close()
    
